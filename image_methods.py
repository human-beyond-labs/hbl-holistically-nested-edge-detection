import numpy as np
import os
import re
import cv2
import blend_modes
from PIL import Image

class CropLayer(object):
    def __init__(self, params, blobs):
        # initialize our starting and ending (x, y)-coordinates of
        # the crop
        self.startX = 0
        self.startY = 0
        self.endX = 0
        self.endY = 0

    def getMemoryShapes(self, inputs):
        # the crop layer will receive two inputs -- we need to crop
        # the first input blob to match the shape of the second one,
        # keeping the batch size and number of channels
        (inputShape, targetShape) = (inputs[0], inputs[1])
        (batchSize, numChannels) = (inputShape[0], inputShape[1])
        (H, W) = (targetShape[2], targetShape[3])

        # compute the starting and ending crop coordinates
        self.startX = int((inputShape[3] - targetShape[3]) / 2)
        self.startY = int((inputShape[2] - targetShape[2]) / 2)
        self.endX = self.startX + W
        self.endY = self.startY + H

        # return the shape of the volume (we'll perform the actual
        # crop during the forward pass
        return [[batchSize, numChannels, H, W]]

    def forward(self, inputs):
        # use the derived (x, y)-coordinates to perform the crop
        return [inputs[0][:, :, self.startY:self.endY,
                        self.startX:self.endX]]


class ImageProcess:
    def __init__(self, args):
        self.args = args

    def read_file(self, path):
        image = cv2.imread(path)
        return image

    def get_canny_edge(self, image):
        """Extrac edges using Canny algorithm
        
        Arguments:
            image {numpy.ndarray} -- OpenCV Image
        
        Returns:
            {numpy.ndarray} -- Edges as RGB image
        """
        cany_threshold_1 = self.args["canny_threshold_1"]
        cany_threshold_2 = self.args["canny_threshold_2"]
        # Generate grayscale image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # Generate edges
        canny = cv2.Canny(gray, cany_threshold_1, cany_threshold_2)
        black_lines = cv2.bitwise_not(canny)
        # Convert into RGB
        black_lines_rgb = cv2.cvtColor(black_lines, cv2.COLOR_GRAY2RGBA)
        # print_image_info("Canny", canny)
        return black_lines_rgb

    def get_kmeans_segment(self, image, k):
        """Generates a segment using K-means
        
        Arguments:
            img {numpy.ndarray} -- An openCV image
            k {int} -- Number of clusters (image bits) used for output
        
        Returns:
            {numpy.ndarray} -- Segment extracted from the image
        """

        Z = image.reshape((-1, 3))
        # convert to float32
        Z = np.float32(Z)
        # # define criteria, number of clusters(K) and apply kmeans()
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        ret, label, center = cv2.kmeans(
            Z, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

        # Convert back into uint8, and make image
        center = np.uint8(center)
        res = center[label.flatten()]
        segment = res.reshape((image.shape))
        # segment_rgba = cv2.cvtColor(segment, cv2.COLOR_RGB2RGBA)

        # Update segment color
        bw_segment = cv2.cvtColor(segment, cv2.COLOR_RGB2GRAY)
        (T, img_threshold) = cv2.threshold(bw_segment, 140, 255, cv2.THRESH_BINARY)
        # segment = cv2.resize(segment[0, 0], (W, H))

        # segment_rgba = cv2.cvtColor(segment, cv2.COLOR_GRAY2RGBA)
        # segment_rgba = cv2.cvtColor(img_threshold, cv2.COLOR_GRAY2RGBA)
        segment_rgba = cv2.cvtColor(img_threshold, cv2.COLOR_GRAY2RGBA)
        colored_segment = self.replace_color(segment_rgba, '#000000', self.args["segment_color"])
        # return segment_rgba
        # TODO: Return the right color (B & R seamed to be flipped)
        return colored_segment

    def get_threshold(self, image):
        thresh = self.args["threshold"]
        max_value = 255
        # Generate grayscale image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # Generate threshold
        (T, img_threshold) = cv2.threshold(gray, thresh, max_value, cv2.THRESH_BINARY)
        # Convert grayscale to RGBA
        segment_rgba = cv2.cvtColor(img_threshold, cv2.COLOR_GRAY2RGBA)
        colored_segment = self.replace_color(segment_rgba, '#000000', self.args["segment_color"])
        # TODO: Return the right color (B & R seamed to be flipped)
        return colored_segment

    def get_hed_edge(self, image):
        """Extract lines using Holistically-Nested Edge Detection
        
        Arguments:
            image {numpy.ndarray} -- OpenCV Image
        
        Returns:
            {numpy.ndarray} -- [description]
        """

        (H, W) = image.shape[:2]
        # load our serialized edge detector from disk
        protoPath = os.path.sep.join([self.args["edge_detector"], "deploy.prototxt"])
        modelPath = os.path.sep.join([self.args["edge_detector"], "hed_pretrained_bsds.caffemodel"])
        net = cv2.dnn.readNetFromCaffe(protoPath, modelPath)

        # register our new layer with the model
        cv2.dnn_registerLayer("Crop", CropLayer)

        # Holistically-Nested Edge detection
        # construct a blob out of the input image for the Holistically-Nested
        # Edge Detector
        blob = cv2.dnn.blobFromImage(image, scalefactor=1.0, size=(W, H),
                                    mean=(104.00698793, 116.66876762, 122.67891434),
                                    swapRB=False, crop=False)

        # set the blob as the input to the network and perform a forward pass
        # to compute the edges
        net.setInput(blob)
        hed = net.forward()
        hed = cv2.resize(hed[0, 0], (W, H))
        hed = (255 * hed).astype("uint8")
        # print(print"HED Image Shape & type: ", hed.shape. )
        return hed

    def write_image_file(self, original_name, image):
        """Creates a file with the given image.
        Uses the path declared at execution
        
        Arguments:
            original_name {str} -- Name of the original image
            sufix {str} -- Sufix to add to the name
            image {numpy.ndarray} -- Image to be saved
        """

        output_directory = self.args["output_directory"]
        name_pattern = re.compile(r'(\w+)\.*')
        image_name = name_pattern.match(original_name)
        # image_prefix = self.args["image-prefix"]
        image_extension = self.args["image_extension"]
        # hed_image_path = "images/ouput/hed.jpeg"
        output_image_path = f"{output_directory}{image_name[1]}{image_extension}"
        # print(f'Output image name: {image_name[1]}')
        try:
            cv2.imwrite(output_image_path, image)
            # cv2.imwrite("images/output/blend.png", canny_kmeans)
        except ValueError as e:
            print(f'[ERROR] Something went wrong with the file: {e}')

    def blend(self, background, foreground):
        background_img = np.array(background)  # Inputs to blend_modes need to be numpy arrays.
        background_img_float = background_img.astype(float) # Inputs to blend_modes need to be floats.
        foreground_img = np.array(foreground)
        foreground_image_float = foreground_img.astype(float)
        blend = blend_modes.multiply(background_img_float, foreground_image_float, 1)
        return blend

    def hex_to_rgb(self, hex_value):
        """Converts Hexadecimal value to RGB format
        
        Arguments:
            hex_value {str} -- #fff or #ffffff
        
        Returns:
            [tuple] -- A tuple in the shape of (255,255,255)
        """
        hex = hex_value.lstrip('#')
        rgb = tuple(int(hex[i:i+2], 16) for i in (0, 2, 4))
        return rgb

    def replace_color(self, image, in_color, out_color):
        in_color_rgb = self.hex_to_rgb(in_color)
        out_color_rgb = self.hex_to_rgb(out_color)
        R,G,B = in_color_rgb
        data = np.array(image)   # "data" is a height x width x 4 numpy array
        red, green, blue, alpha = data.T # Temporarily unpack the bands for readability
        # Replace white with red... (leaves alpha values alone...)
        white_areas = (red == R) & (green == G) & (blue == B)
        data[..., :-1][white_areas.T] = out_color_rgb # Transpose back needed
        # colored_image = Image.fromarray(data)
        return data

    def display_images(self, image_list):
        images_tuple = tuple(image_list)
        # Join images to be display side by side
        display_images = np.hstack(images_tuple)
        cv2.imshow("Blend", display_images)
        cv2.waitKey(0)
        cv2.distroyAllWindows()

    # def update_brightness(self, img, value):
    #     hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    #     h, s, v = cv2.split(hsv)

    #     lim = 255 - value
    #     v[v > lim] = 255
    #     v[v <= lim] += value
    #     final_hsv = cv2.merge((h, s, v))
    #     image = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    #     # updated_image = cv2.add(hsv[:,:,2], value, hsv[:,:,2])
    #     # image = cv2.cvtColor(updated_image, cv2.COLOR_HSV2BGR)

    #     return image

    # def update_contrast(self, image, value):
        

# Merge the hed and canny edges into one imagechange
# hed_canny = cv2.add(hed, canny)
# Add alpha channel
# hed_canny = cv2.cvtColor(hed_canny, cv2.COLOR_GRAY2RGBA)

####################################################
# 1_ Invert edges to get white background
# 2_ Make white pixels transparent
# 3_ Overlay Edges on top of Segment
################ Blend Segment & Edges #############
# Invert
# hed_canny = cv2.bitwise_not(hed_canny) # This actually creates an interesting mask of white background
# Generate and apply a mask for white pixels
# hed_canny[np.all(hed_canny == [0, 0, 0, 255], axis=2)] = [0, 0, 0, 0]

# Generate and apply a mask for black pixels
# hed_canny[np.all(hed_canny == [0, 0, 0, 255], axis=2)] = [0, 0, 0, 0]
# hed_canny[hed_canny_inverted] = [0, 0, 0, 0]
# ret, mask = cv2.threshold(hed_canny, 50, 255, cv2.THRESH_BINARY)
# mask_inv = cv2.bitwise_not(mask)
# print("Mask Image Size:", mask.shape)
# Now black-out the area in the segment (background)
# img1_bg = cv2.bitwise_and(segment, segment, mask = mask_inv)
# Take only region of logo from logo image.
# img2_fg = cv2.bitwise_and(hed_canny,hed_canny,mask = mask)
# Put logo in ROI and modify the main image
# hed_canny_kmeans = cv2.add(img1_bg,img2_fg)
# img1[0:rows, 0:cols ] = dst

# Create a black placeholder
# hed_canny_kmeans = np.zeros((512,512,4), np.uint8) # transparent with alpha channel
# canny_kmeans = np.zeros((512,512,3), np.uint8)

# Add edges to segment
# hed_canny_kmeans = cv2.add(hed_canny, segment)
# canny_kmeans = cv2.add(canny_rgb, segment)

# Use Blend modes
# Transform to Float32
# segment = np.float32(segment / 255)
# hed_canny = np.float32(hed_canny / 255)
# opacity = .9  # The opacity of the foreground that is blended onto the background is 70 %.
# hed_canny_kmeans = blend_modes.overlay(segment, hed_canny, opacity)
# r,g,b,a = cv2.split(hed_canny)
# overlay_color = cv2.merge((r,g,b))


# Weighted addition
# hed_canny = cv2.bitwise_not(hed_canny)
# hed_canny_kmeans = cv2.addWeighted(segment, 0.8, hed_canny, 0.2, 0)


# Get black pixels on transparent background
# hed_canny_kmeans = cv2.add(hed_canny, segment)
#####################################################