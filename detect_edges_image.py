# USAGE
# python detect_edges_image.py --edge-detector hed_model --image images/guitar.jpg
import numpy as np
import cv2
from tqdm import tqdm
from config import parse_arguments
from image_methods import ImageProcess
from utils import print_image_info, get_file_list

# Parse arguments
args = parse_arguments()

# Load image processing class
image_process = ImageProcess(args)

# Load images
if args["image"]:
    # Create a list of a single image
    image = [cv2.imread(args["image"])]
    ## TODO: Add conditional to process single image
    # print_image_info("Input image", image)
elif args["input_directory"]:
    # Generate Image List
    image_list = get_file_list(args["input_directory"]) 

# Loop through list extracting Edge & Segment
for file_name in tqdm(image_list):
    # Load image from file
    full_path = f'{args["input_directory"]}{file_name}'
    image = image_process.read_file(full_path)
    
    # Modify brightness level
    # if args["brightness"] is not None:
    #     image = image_process.update_brightness(image, args["brightness"])
    
    # # Modify contrast level
    # if args["contrast"] is not None:
    #     image = image_process.increase_contrast(image, args["contrast"])
    
    # Blure the image to have smooth edges
    blured_image = cv2.GaussianBlur(image, (args["blur_value"],args["blur_value"]), 0)
    
    # Extract Canny Edge
    canny = image_process.get_canny_edge(blured_image)
    # print_image_info("Canny Edge", canny)

    # Extract Segment using K-means Clustering
    segment = image_process.get_kmeans_segment(blured_image, 2)
    # print_image_info("K-means", segment)

    # Extract segment using pixel threshold
    # threshold = image_process.get_threshold(image)
    # print_image_info("Threshold", threshold)
    
    # Extract HED Edge
    # hed = image_process.get_hed_edge(image)
    # print_image_info("HED", hed)

    # Blend Edge & Segment
    blend = image_process.blend(segment, canny)

    # Check if we should save the image or display it
    if args["save_image"]:
        # Define what to save
        if args["side_by_side"]:
            # Concatenate images
            image_rgba = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
            a_b = np.hstack((image_rgba, blend))
            image_process.write_image_file(file_name, a_b)
        else:
            image_process.write_image_file(file_name, blend)
            
    elif args["display_images"]:
        # Define what to show
        # image_rgba = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
        image_process.display_images([blend])

if args["save_image"]:
    print(f'Images saved in directory: {args["output_directory"]}')