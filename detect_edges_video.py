# USAGE
# python detect_edges_video.py --edge-detector hed_model

# import the necessary packages
from __future__ import print_function
# from imutils.video import VideoStream
from imutils.video import WebcamVideoStream
from imutils.video import FPS
import argparse
import imutils
import time
import cv2
import os
import numpy as np

# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--edge-detector", type=str, required=True,
                help="path to OpenCV's deep learning edge detector")
ap.add_argument("-n", "--num-frames", type=int, default=100,
                help="# of frames to loop over for FPS test")
ap.add_argument("-i", "--input", type=str,
                help="path to optional input video (webcam will be used otherwise)")
# ap.add_argument("-f", "--display-fps", action='store_true',
# 	help="If declared the output image will be save to a file in images/output")
args = vars(ap.parse_args())


class CropLayer(object):
    def __init__(self, params, blobs):
        # initialize our starting and ending (x, y)-coordinates of
        # the crop
        self.startX = 0
        self.startY = 0
        self.endX = 0
        self.endY = 0

    def getMemoryShapes(self, inputs):
        # the crop layer will receive two inputs -- we need to crop
        # the first input blob to match the shape of the second one,
        # keeping the batch size and number of channels
        (inputShape, targetShape) = (inputs[0], inputs[1])
        (batchSize, numChannels) = (inputShape[0], inputShape[1])
        (H, W) = (targetShape[2], targetShape[3])

        # compute the starting and ending crop coordinates
        self.startX = int((inputShape[3] - targetShape[3]) / 2)
        self.startY = int((inputShape[2] - targetShape[2]) / 2)
        self.endX = self.startX + W
        self.endY = self.startY + H

        # return the shape of the volume (we'll perform the actual
        # crop during the forward pass
        return [[batchSize, numChannels, H, W]]

    def forward(self, inputs):
        # use the derived (x, y)-coordinates to perform the crop
        return [inputs[0][:, :, self.startY:self.endY,
                          self.startX:self.endX]]


def getKmeansSegment(img, k):
    Z = img.reshape((-1, 3))
    # convert to np.float32
    Z = np.float32(Z)
    # # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret, label, center = cv2.kmeans(
        Z, k, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))
    return res2


# initialize a boolean used to indicate if either a webcam or input
# video is being used
webcam = not args.get("input", False)

# if a video path was not supplied, grab a reference to the webcam
if webcam:
    print("[INFO] starting video stream...")
    vs = WebcamVideoStream(src=0).start()
    time.sleep(2.0)

# otherwise, grab a reference to the video file
else:
    print("[INFO] opening video file...")
    vs = cv2.VideoCapture(args["input"])

fps = FPS().start()

# load our serialized edge detector from disk
print("[INFO] loading edge detector...")
protoPath = os.path.sep.join([args["edge_detector"],
                              "deploy.prototxt"])
modelPath = os.path.sep.join([args["edge_detector"],
                              "hed_pretrained_bsds.caffemodel"])
net = cv2.dnn.readNetFromCaffe(protoPath, modelPath)

# register our new layer with the model
cv2.dnn_registerLayer("Crop", CropLayer)

# Start timer
# start_time = time.time()
x = 1  # displays the frame rate every 1 second
counter = 0

# loop over frames from the video stream
while True:
    # grab the next frame and handle if we are reading from either
    # VideoCapture or VideoStream
    frame = vs.read()
    frame = frame if webcam else frame[1]
    # if we are viewing a video and we did not grab a frame then we
    # have reached the end of the video
    if not webcam and frame is None:
        break

    # resize the frame and grab its dimensions
    frame = imutils.resize(frame, width=512)
    (H, W) = frame.shape[:2]

    # convert the frame to grayscale, blur it, and perform Canny
    # edge detection
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    canny = cv2.Canny(blurred, 30, 150)

    # construct a blob out of the input frame for the Holistically-Nested
    # Edge Detector, set the blob, and perform a forward pass to
    # compute the edges
    blob = cv2.dnn.blobFromImage(frame, scalefactor=1.0, size=(W, H),
                                 mean=(104.00698793, 116.66876762, 122.67891434),
                                 swapRB=False, crop=False)
    net.setInput(blob)
    hed = net.forward()
    hed = cv2.resize(hed[0, 0], (W, H))
    hed = (255 * hed).astype("uint8")

    # Merge the hed and canny edges into one image
    hed_plus_canny = cv2.add(hed, canny)

    # K-means Segment detection
    segment = getKmeansSegment(frame, 4)
    # show the output edge detection results for Canny and
    # Holistically-Nested Edge Detection
    # cv2.imshow("Frame", frame)
    # cv2.imshow("Canny", canny)
    # cv2.imshow("HED", hed)
    cv2.imshow("K-means Segment", segment)
    cv2.imshow("HED + Canny", hed_plus_canny)
    key = cv2.waitKey(1) & 0xFF

    # Manual FPS counter
    # counter += 1
    # if (time.time() - start_time) > x:
    # 	print("FPS: ", counter / (time.time() - start_time))
    # 	counter = 0
    # 	start_time = time.time()

    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break

    # check to see if the frame should be displayed to our screen
    # if args["display_fps"] > 0:
    # 	cv2.imshow("Frame", frame)
    # 	key = cv2.waitKey(1) & 0xFF

    # update the FPS coundisplayter
    fps.update()

fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# if we are using a webcam, stop the camera video stream
if webcam:
    # do a bit of cleanup
    cv2.destroyAllWindows()
    vs.stop()

# otherwise, release the video file pointer
else:
    vs.release()

# close any open windows
cv2.destroyAllWindows()
