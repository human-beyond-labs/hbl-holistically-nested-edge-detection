from pathlib import Path

def print_image_info(name, image):
        """Prints Shape & Type
        
        Arguments:
            name {string} -- Name of the image
            image {object} -- OpenCV image
        """
        print(f'{name} shape: {image.shape}')
        print(f'{name} dtype: {image.dtype}')
        print(f"{name} type: {type(image)}")

def get_file_list(dir):
    file_list = []
    for filename in Path(dir).glob('**/*.*'):
        file_list.append(filename.name)
        # Add just the filename without the extension
        # file_list.append(filename.stem)
    # print(file_list)
    return file_list