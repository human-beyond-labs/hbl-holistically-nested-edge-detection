# hbl-holistically-nested-edge-detection
A CLI tool to extract edges from images using HED

## Basic usage

### Images
```bash
python detect_edges_image.py --input_directory input_directory/ output_directory/ -c '#888888' --threshold 190 -s
```
### Video
```bash
python detect_edges_video.py --edge-detector hed_model
```

### Examples

### Gummy Strings

```bash
python detect_edges_image.py --input-directory augmented_512/ --output-directory edges_512/ -c '#2f2cd4' -s
```

### Gummy Bears Macro
```bash
python detect_edges_image.py --input-directory test_augmented_512/ --output-directory test_edges_512/ -c '#942ec7' -l 75 --canny-threshold-1 120 --canny-threshold-2 160 -b 7 -s
```
### Bread Mix (Using K-means)
```bash
python detect_edges_image.py --input-directory test_bread_mix_augmented_512/ --output-directory test_bread_mix_edges_512/ -c '#c7b52e' -l 190 --canny-threshold-1 80 --canny-threshold-2 100 -b 9 -s --side-by-side
```