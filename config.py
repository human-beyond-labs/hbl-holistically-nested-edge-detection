import argparse

def do_colab_check():
    """Verify if we're in a colaboratory notebook

    Returns:
        bool
    """
    try:
        import google.colab
        return True
    except:
        return False

def parse_arguments():
    """Parses CLI arguments and returns paths and config data
    
    Returns:
        object -- Paths and config data
    """
    ap = argparse.ArgumentParser()
    ap.add_argument("-e", "--edge-detector", type=str,
                    help="path to OpenCV's deep learning edge detector")
    ap.add_argument("-f", "--image", type=str, required=False,
                    help="path to input image")
    ap.add_argument("-i", "--input-directory", type=str, required=False,
                    help="path to input image directory")
    ap.add_argument("-p", "--image-prefix", type=str, default="__", required=False,
                    help="prefix to add to the output image files")
    ap.add_argument("-x", "--image-extension", type=str, default=".jpeg", required=False,
                    help="the suffix to use as file extension")
    ap.add_argument("-o", "--output-directory", type=str, default="images/output/" , required=False,
                    help="output directory to create the image files")
    ap.add_argument("-w", "--display-images", action='store_true', required=False,
                    help="display generated images in a popup window")
    ap.add_argument("-s", "--save-image", action='store_true',
                    help="If declared the output image will be save to a file in images/output")
    ap.add_argument("-c", "--segment-color", type=str, default="#b3b3b3",
                    help="The color of the extracted segment in hex")
    ap.add_argument("-l", "--threshold", type=int, default=190,
                    help="The color of the extracted segment in hex")
    ap.add_argument("--canny-threshold-1", type=int, default=40,
                    help="Lower limit for Canny edge extraction")
    ap.add_argument("--canny-threshold-2", type=int, default=150,
                    help="higher limit for Canny edge extraction")
    ap.add_argument("-b", "--blur_value", type=int, default=9,
                    help="Gaussian blur value (Has to be an odd number)")
    ap.add_argument("--side-by-side", action='store_true',
                    help="Creates a file with original image next to the edges")
    # ap.add_argument("-v", "--brightness", type=int,
    #                 help="Change global brightness value")

    args = vars(ap.parse_args())
    return args